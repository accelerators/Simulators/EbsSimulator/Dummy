# Dummy

A dummy class which implements a Tango class with
no commands and no attributes. It is used in the EBS
simulators to simulate device not simulated but required by some GUIs

## Cloning

To clone this projet, type the following command

```
git clone git@gitlab.esrf.fr:accelerators/Simulators/EbsSimulator/Dummy.git
```

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies 

* Tango Controls 9 or higher.
* omniORB release 4 or higher.
* libzmq - libzmq3-dev or libzmq5-dev.

#### Toolchain Dependencies

* C++11 compliant compiler.
* CMake 3.0 or greater is required to perform the build.

### Build

Instructions on building the project.

CMake example:

```bash
cd Dummy
mkdir -p build/<os>
cd build/<os>
cmake ../..
make
```
